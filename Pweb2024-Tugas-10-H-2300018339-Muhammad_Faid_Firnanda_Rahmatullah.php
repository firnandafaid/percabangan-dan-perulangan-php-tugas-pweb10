<?php
$nim = 2300018339;
$nama = "Muhammad Faid";
$nilai = 90;

echo "============Listing Program 10.2.1============<br>";
echo "Nim: $nim<br>";
echo "Nama: $nama<br>";

if ($nilai >= 80 && $nilai <=100 ) {
    echo "Nilai: $nilai (A) , Lulus." . "<br>";
} elseif ($nilai >= 65 && $nilai < 80) {
    echo "Nilai: $nilai (B) , Lulus." . "<br>";
} elseif ($nilai >= 50 && $nilai < 65) {
    echo "Nilai: $nilai (C) , Lulus" . "<br>";
} elseif ($nilai >= 25 && $nilai < 50) {
    echo "Nilai : $nilai (D) , Tidak Lulus" . "<br>";
} elseif ($nilai >= 0 && $nilai < 25 ) {
    echo "Nilai : $nilai (E) , Tidak Lulus" . "<br>";
}else{
    echo "Nilai yang anda masukkan: $nilai<br>Tolong inputkan nilai dengan benar!<br>";
}

echo "<br><br>============Listing Program 10.2.2============<br>";

$tinggi = 5;
for ($baris = 1; $baris <= $tinggi; $baris++) {
    for ($i = 1; $i <= $tinggi - $baris; $i++) {
        echo "&nbsp"; //$nbsp disini adalah karakter untuk "spasi" di dalam PHP.
    }
    for ($j = 1; $j < 2 * $baris; $j++) {
        echo "*";
    }
    echo "<br>";
}
?>